import 'package:localstorage/localstorage.dart';

class UserStorageService {
  static const String _storageKey = 'stored_user_information';
  static const String _userNameKey = 'stored_user_information_user_name';
  static const String _passwordKey = 'stored_user_information_password';
  final LocalStorage _storage = new LocalStorage(_storageKey);
  late Future storageReady;

  UserStorageService() {
    storageReady = _initialize();
  }

  Future _initialize() async {
    await _storage.ready;
    var userName = _storage.getItem(_userNameKey);
    if (userName == null) {
      await _storage.setItem(_userNameKey, "");
    }
    var password = _storage.getItem(_passwordKey);
    if (password == null) {
      await _storage.setItem(_passwordKey, "");
    }
  }

  Future clearStorage() async {
    await _storage.clear();
    await _storage.setItem(_userNameKey, "");
    await _storage.setItem(_passwordKey, "");
  }

  Future<String> getUserName() async {
    await storageReady;
    return await _storage.getItem(_userNameKey);
  }

  Future<String> getPassword() async {
    await storageReady;
    return await _storage.getItem(_passwordKey);
  }

  Future updateUserName(String userName) async {
    await _storage.setItem(_userNameKey, userName);
  }

  Future updatePassword(String password) async {
    await _storage.setItem(_passwordKey, password);
  }
}
