import 'package:flutter/material.dart';
import 'package:planning_assistant/model/routing_mode.dart';

DateTime getDate(DateTime dateTime) {
  return new DateTime(dateTime.year, dateTime.month, dateTime.day);
}

List<T> flatten<T>(Iterable<Iterable<T>> list) =>
    [for (var sublist in list) ...sublist];

Color getColorByLong(long) {
  List<Color> colors = [
    const Color(0xffb74093),
    const Color(0xff689B59),
    const Color(0xff66B9D3),
    const Color(0xff8C80D9),
    const Color(0xffE95BAD),
    const Color(0xffF6896A),
    const Color(0xffF6BA6A),
    const Color(0xff00A651),
    const Color(0xff0066B3),
    const Color(0xff8441B6),
    const Color(0xffE80689),
    const Color(0xffED1C24),
    const Color(0xffF7941E),
    const Color(0xff9CCC44),
    const Color(0xff2EB3FF),
    const Color(0xff9E98C5),
    const Color(0xffFF95D8),
    const Color(0xffFF637E),
    const Color(0xffDE991C),
    const Color(0xff04FF7F),
    const Color(0xff0995FF),
    const Color(0xff9207FB),
    const Color(0xffFF0696),
    const Color(0xffFF050F),
    const Color(0xffFF8C03),
    const Color(0xffAEF598),
    const Color(0xff7EDFFE),
    const Color(0xffA496FF),
    const Color(0xffFF67BF),
    const Color(0xffFDAB94),
    const Color(0xffFFCC87),
    const Color(0xff8BA163),
    const Color(0xff608FA9),
    const Color(0xff5A4DAA),
    const Color(0xffB85C96),
    const Color(0xffB14558),
    const Color(0xffD29E3E),
    const Color(0xff009976),
    const Color(0xff317FC9),
    const Color(0xff9B61B4),
    const Color(0xffD23991),
    const Color(0xffDE5C69),
    const Color(0xffF6BA0D),
    const Color(0xff9ED439),
    const Color(0xff069EF4),
    const Color(0xffB161ED),
    const Color(0xffFE45B0),
    const Color(0xffEE294B),
    const Color(0xffF0A33D),
    const Color(0xff4AF4CD),
    const Color(0xff7BA1C5),
    const Color(0xffCA77EE),
    const Color(0xffEAAFD1),
    const Color(0xffBF747C),
    const Color(0xffF1CE6A),
    const Color(0xff92DD09),
    const Color(0xff329CD9),
    const Color(0xff8E44C7),
    const Color(0xffDB65A9),
    const Color(0xff96646D),
    const Color(0xffCBAC84),
  ];
  var abs = long.abs();
  return colors[abs % colors.length];
}

String getRoutingModeString(RoutingMode mode) {
  return mode.toString().split('.').last;
}