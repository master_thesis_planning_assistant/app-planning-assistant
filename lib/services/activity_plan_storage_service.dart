import 'dart:convert';

import 'package:localstorage/localstorage.dart';
import 'package:planning_assistant/model/activity_plan.dart';

class ActivityPlanStorageService {
  static const String _storageKey = 'stored_activity_plans';
  static const String _masterKey = 'stored_activity_plans_master';
  final LocalStorage _storage = new LocalStorage(_storageKey);
  late Future storageReady;

  ActivityPlanStorageService() {
    storageReady = _initialize();
  }

  Future _initialize() async {
    await _storage.ready;
    var masterEntry = _storage.getItem(_masterKey);
    if (masterEntry == null) {
      List<String> masterEntry = [];
      await _storage.setItem(_masterKey, jsonEncode(masterEntry));
    }
  }

  Future clearStorage() async {
    await _storage.clear();
    List<String> masterEntry = [];
    await _storage.setItem(_masterKey, jsonEncode(masterEntry));
  }

  Future saveActivityPlanToStorage(ActivityPlan plan) async {
    await _storage.setItem(plan.guid, jsonEncode(plan));
    var masterEntry = jsonDecode(await _storage.getItem(_masterKey));
    masterEntry.add(plan.guid);
    await _storage.setItem(_masterKey, jsonEncode(masterEntry));
  }

  Future removeActivityPlanFromStorage(String guid) async {
    await _storage.deleteItem(guid);
    List<dynamic> masterEntry = jsonDecode(await _storage.getItem(_masterKey));
    masterEntry.removeWhere((element) => element == guid);
    await _storage.setItem(_masterKey, jsonEncode(masterEntry));
  }

  Future<List<ActivityPlan>> getAllActivityPlans() async {
    var masterEntry = jsonDecode(await _storage.getItem(_masterKey));
    List<ActivityPlan> activityPlans = [];
    await Future.forEach(masterEntry, (key) async {
      var planData = jsonDecode(await _storage.getItem(key as String));
      if (planData != null) activityPlans.add(ActivityPlan.fromJson(planData));
    });
    return activityPlans;
  }
}
