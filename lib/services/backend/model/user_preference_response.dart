import 'package:json_annotation/json_annotation.dart';
import 'package:planning_assistant/model/address.dart';
import 'package:planning_assistant/model/preferences/preferences.dart';

part 'user_preference_response.g.dart';

@JsonSerializable(explicitToJson: true)
class UserPreferenceResponse {
  final List<Preferences> preferences;
  final Address homeAddress;

  UserPreferenceResponse(this.preferences, this.homeAddress);

  factory UserPreferenceResponse.fromJson(Map<String, dynamic> json) =>
      _$UserPreferenceResponseFromJson(json);

  Map<String, dynamic> toJson() => _$UserPreferenceResponseToJson(this);
}
