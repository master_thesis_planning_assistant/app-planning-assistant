import 'package:json_annotation/json_annotation.dart';

part 'user_preference_request.g.dart';

@JsonSerializable(explicitToJson: true)
class UserPreferenceRequest {
  final String userName;
  final String password;

  UserPreferenceRequest(this.userName, this.password);

  factory UserPreferenceRequest.fromJson(Map<String, dynamic> json) =>
      _$UserPreferenceRequestFromJson(json);

  Map<String, dynamic> toJson() => _$UserPreferenceRequestToJson(this);
}
