import 'package:json_annotation/json_annotation.dart';
import 'package:planning_assistant/model/activity_plan.dart';

part 'activity_plan_response.g.dart';

@JsonSerializable(explicitToJson: true)
class ActivityPlanResponse {
  final List<ActivityPlan> activityPlans;

  ActivityPlanResponse(this.activityPlans);

  factory ActivityPlanResponse.fromJson(Map<String, dynamic> json) =>
      _$ActivityPlanResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ActivityPlanResponseToJson(this);
}
