import 'package:json_annotation/json_annotation.dart';
import 'package:planning_assistant/model/activity.dart';

part 'activity_plan_request.g.dart';

@JsonSerializable(explicitToJson: true)
class ActivityPlanRequest {
  final List<Activity> activities;
  final String userName;
  final String password;
  final DateTime day;
  final bool filterRouteDuplicates;

  ActivityPlanRequest(this.activities, this.day, this.userName, this.password,
      this.filterRouteDuplicates);

  factory ActivityPlanRequest.fromJson(Map<String, dynamic> json) =>
      _$ActivityPlanRequestFromJson(json);

  Map<String, dynamic> toJson() => _$ActivityPlanRequestToJson(this);
}
