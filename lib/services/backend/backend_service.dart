import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:planning_assistant/config.dart';
import 'package:planning_assistant/model/activity.dart';
import 'package:planning_assistant/model/activity_plan.dart';
import 'package:planning_assistant/services/backend/model/activity_plan_request.dart';
import 'package:planning_assistant/services/backend/model/activity_plan_response.dart';
import 'package:planning_assistant/services/backend/model/user_preference_request.dart';
import 'package:planning_assistant/services/backend/model/user_preference_response.dart';

Future<List<ActivityPlan>> getActivityPlan(
    List<Activity> activities,
    DateTime day,
    String userName,
    String password,
    bool filterRouteDuplicates) async {
  var uri = Uri.http(EnvironmentConfig.BACKEND_BASE_URI, 'activityPlan');

  var request = ActivityPlanRequest(
      activities, day, userName, password, filterRouteDuplicates);

  final response = await http.post(
    Uri.parse(uri.toString()),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(request),
  );

  if (response.statusCode == 200) {
    var responseObject =
        ActivityPlanResponse.fromJson(jsonDecode(response.body));
    return responseObject.activityPlans;
  } else {
    throw new Exception('Error Fetching activity plans');
  }
}

Future<UserPreferenceResponse> getUserPreferences(
    String userName, String password) async {
  var uri = Uri.http(EnvironmentConfig.BACKEND_BASE_URI, 'preferences');
  var request = UserPreferenceRequest(userName, password);

  final response = await http.put(
    Uri.parse(uri.toString()),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(request),
  );

  if (response.statusCode == 200) {
    var responseObject =
        UserPreferenceResponse.fromJson(jsonDecode(response.body));
    return responseObject;
  } else {
    throw new Exception('Error Fetching user preferences');
  }
}
