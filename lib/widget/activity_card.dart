import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:planning_assistant/model/activity.dart';
import 'package:planning_assistant/services/utility.dart';

class ActivityCard extends StatelessWidget {
  const ActivityCard({Key? key, required this.activity, required this.delete})
      : super(key: key);

  final Activity activity;
  final Function delete;

  @override
  Widget build(BuildContext context) {
    var timeFlexible = activity.startTime == null ||
        activity.startTime == getDate(activity.startTime!);

    return Card(
      margin: EdgeInsets.fromLTRB(16, 16, 16, 0),
      child: Padding(
        padding: EdgeInsets.all(12),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                timeFlexible
                    ? Text('Flexiebel',
                        style: Theme.of(context).textTheme.caption)
                    : Text(DateFormat('HH:mm').format(activity.startTime!)),
                Text(
                  activity.title,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                Text((activity.duration / 60).round().toString() + ' min'),
              ],
            ),
            SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    IconButton(
                      iconSize: 20,
                      splashRadius: 25,
                      color: Colors.red,
                      onPressed: () {
                        delete();
                      },
                      icon: Icon(Icons.delete),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      activity.address != null
                          ? activity.address!.streetAndNumber!
                          : 'Variabler Ort',
                      style: Theme.of(context).textTheme.caption,
                    ),
                    Text(
                      activity.address != null
                          ? activity.address!.postalCode! +
                              ' ' +
                              activity.address!.city!
                          : '',
                      style: Theme.of(context).textTheme.caption,
                    ),
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
