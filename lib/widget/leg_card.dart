import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:planning_assistant/model/leg.dart';
import 'package:planning_assistant/services/utility.dart';
import 'package:planning_assistant/widget/steps_card.dart';

class LegCard extends StatelessWidget {
  const LegCard({Key? key, required this.leg}) : super(key: key);

  final Leg leg;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.fromLTRB(16, 8, 16, 8),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  getRoutingModeString(leg.modality),
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                Text(leg.description != null ? leg.description! : "")
              ],
            ),
            SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Dauer: " + leg.duration.round().toString() + " s",
                      style: Theme.of(context).textTheme.caption,
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      "Strecke: " + leg.distance.round().toString() + " m",
                      style: Theme.of(context).textTheme.caption,
                    ),
                  ],
                )
              ],
            ),
            SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Ab: " +
                          DateFormat('HH:mm').format(leg.departure.toLocal()),
                      style: Theme.of(context).textTheme.caption,
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      "An: " +
                          DateFormat('HH:mm').format(leg.arrival.toLocal()),
                      style: Theme.of(context).textTheme.caption,
                    ),
                  ],
                )
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Start: " +
                      (leg.startLocation.name == null
                          ? "-"
                          : leg.startLocation.name!),
                  style: Theme.of(context).textTheme.caption,
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  "Ziel: " +
                      (leg.endLocation.name == null
                          ? "-"
                          : leg.endLocation.name!),
                  style: Theme.of(context).textTheme.caption,
                ),
              ],
            ),
            SizedBox(height: 5),
            StepsCard(steps: leg.steps)
          ],
        ),
      ),
    );
  }
}
