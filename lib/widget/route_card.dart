import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:planning_assistant/model/plan_route.dart';
import 'package:planning_assistant/page/subpages/route_details_page.dart';
import 'package:planning_assistant/services/utility.dart';

class RouteCard extends StatelessWidget {
  const RouteCard({Key? key, required this.route, required this.title})
      : super(key: key);

  final PlanRoute route;
  final String title;

  void showDetails(context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => RouteDetailPage(
          route: route,
          title: title,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.fromLTRB(16, 8, 16, 8),
      child: InkWell(
        onTap: () => showDetails(context),
        child: Padding(
          padding: EdgeInsets.all(12),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        height: 10.0,
                        width: 10.0,
                        color: route.getRouteColor(),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        title,
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ],
                  ),
                  Text(
                    route.legs
                        .map((l) => getRoutingModeString(l.modality))
                        .toSet()
                        .toList()
                        .join(", "),
                    style: Theme.of(context).textTheme.caption,
                  )
                ],
              ),
              Divider(
                thickness: 1.3,
              ),
              SizedBox(height: 5),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Ab: " +
                            DateFormat('HH:mm')
                                .format(route.departure.toLocal()),
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        "An: " +
                            DateFormat('HH:mm').format(route.arrival.toLocal()),
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ],
                  )
                ],
              ),
              SizedBox(height: 8),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Dauer: " +
                            new Duration(seconds: route.duration.round())
                                .inMinutes
                                .toString() +
                            " min",
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        "Strecke: " +
                            (route.distance / 1000).toStringAsFixed(1) +
                            " km",
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ],
                  )
                ],
              ),
              SizedBox(height: 8),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Start",
                        style: Theme.of(context).textTheme.caption,
                      ),
                      Text(
                        route.start.streetAndNumber ?? "-",
                        style: Theme.of(context).textTheme.caption,
                      ),
                      Text(
                        (route.start.postalCode ?? "-") +
                            " " +
                            (route.start.city ?? "-"),
                        style: Theme.of(context).textTheme.caption,
                      )
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        "Ziel",
                        style: Theme.of(context).textTheme.caption,
                      ),
                      Text(
                        route.destination.streetAndNumber ?? "-",
                        style: Theme.of(context).textTheme.caption,
                      ),
                      Text(
                        (route.destination.postalCode ?? "-") +
                            " " +
                            (route.destination.city ?? "-"),
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ],
                  )
                ],
              ),
              Center(
                child: Text(
                  route.serviceName,
                  style: Theme.of(context).textTheme.overline,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
