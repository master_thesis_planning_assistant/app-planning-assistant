import 'package:collection/collection.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:planning_assistant/model/step.dart';

class StepsCard extends StatelessWidget {
  const StepsCard({Key? key, required this.steps}) : super(key: key);

  final List<PlanStep> steps;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ExpandablePanel(
          header: Padding(
            padding: const EdgeInsets.all(10),
            child: Text(
              steps.length.toString() + " - Schritte",
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          collapsed: SizedBox(),
          expanded: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                ...steps.mapIndexed(
                  (j, step) => Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Schritt " + j.toString(),
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                          Text(step.action != null ? step.action! : ""),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Dauer: " +
                                    step.duration.round().toString() +
                                    " s",
                                style: Theme.of(context).textTheme.caption,
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                "Strecke: " +
                                    step.distance.round().toString() +
                                    " m",
                                style: Theme.of(context).textTheme.caption,
                              ),
                            ],
                          )
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Start: " +
                                (step.startLocation!.name == null
                                    ? "-"
                                    : step.startLocation!.name!),
                            style: Theme.of(context).textTheme.caption,
                            softWrap: true,
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            "Ziel: " +
                                (step.endLocation!.name == null
                                    ? "-"
                                    : step.endLocation!.name!),
                            style: Theme.of(context).textTheme.caption,
                            softWrap: true,
                          ),
                        ],
                      ),
                      j < steps.length - 1 ? Divider() : SizedBox()
                    ],
                  ),
                )
              ],
            ),
          )),
    );
  }
}
