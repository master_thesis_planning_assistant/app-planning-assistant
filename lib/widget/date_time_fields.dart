import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class BasicDateField extends StatelessWidget {
  BasicDateField({Key? key, this.text = '', this.initialValue, this.onChanged})
      : super(key: key);
  final String text;
  final format = DateFormat("dd.MM.yyyy");
  final DateTime? initialValue;
  final Function? onChanged;

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      DateTimeField(
        onChanged: onChanged != null ? (value) => onChanged!(value) : null,
        format: format,
        decoration: InputDecoration(
          labelText: text + ' (${format.pattern})',
          icon: Icon(Icons.today),
        ),
        onShowPicker: (context, currentValue) {
          return showDatePicker(
              context: context,
              cancelText: 'Abbrechen',
              confirmText: 'Ok',
              firstDate: DateTime(1900),
              initialDate: currentValue ?? initialValue ?? DateTime.now(),
              lastDate: DateTime(2100));
        },
      ),
    ]);
  }
}

class BasicTimeField extends StatelessWidget {
  BasicTimeField({Key? key, this.text = '', this.onSave}) : super(key: key);
  final String text;
  final format = DateFormat("HH:mm");
  final Function? onSave;

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      DateTimeField(
        format: format,
        decoration: InputDecoration(
          labelText: text + ' (${format.pattern})',
          icon: Icon(Icons.watch_later),
        ),
        onSaved: onSave != null ? (value) => onSave!(value) : null,
        onShowPicker: (context, currentValue) async {
          final TimeOfDay? time = await showTimePicker(
            context: context,
            initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
            cancelText: 'Abbrechen',
            helpText: text,
            confirmText: 'Ok',
            builder: (context, child) => MediaQuery(
                data: MediaQuery.of(context)
                    .copyWith(alwaysUse24HourFormat: true),
                child: child!),
          );
          return time == null ? null : DateTimeField.convert(time);
        },
      ),
    ]);
  }
}

class BasicDateTimeField extends StatelessWidget {
  BasicDateTimeField({Key? key, this.text = ''}) : super(key: key);
  final String text;
  final format = DateFormat("dd.MM.yyyy HH:mm");

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      DateTimeField(
        decoration: InputDecoration(
          labelText: text + ' (${format.pattern})',
          icon: Icon(Icons.today),
        ),
        format: format,
        onShowPicker: (context, currentValue) async {
          final date = await showDatePicker(
              context: context,
              firstDate: DateTime(1900),
              initialDate: currentValue ?? DateTime.now(),
              lastDate: DateTime(2100));
          if (date != null) {
            final time = await showTimePicker(
              context: context,
              initialTime:
                  TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
            );
            return DateTimeField.combine(date, time);
          } else {
            return currentValue;
          }
        },
      ),
    ]);
  }
}
