import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:planning_assistant/model/arranged_activity.dart';
import 'package:planning_assistant/page/subpages/arranged_activity_details_page.dart';

class ArrangedActivityCard extends StatelessWidget {
   const ArrangedActivityCard({Key? key, required this.arrangedActivity})
      : super(key: key);

  final ArrangedActivity arrangedActivity;

  void showDetails(context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) =>
            ArrangedActivityDetails(arrangedActivity: arrangedActivity),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.fromLTRB(16, 8, 16, 8),
      child: InkWell(
        onTap: () => showDetails(context),
        child: Padding(
            padding: EdgeInsets.all(12),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                        DateFormat('HH:mm').format(arrangedActivity.startTime)),
                    Row(
                      children: [
                        Container(
                          height: 10.0,
                          width: 10.0,
                          color: arrangedActivity.route.getRouteColor(),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          arrangedActivity.title,
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ],
                    ),
                    Text((arrangedActivity.duration / 60).round().toString() +
                        ' min'),
                  ],
                ),
                Divider(
                  thickness: 1.3,
                ),
                SizedBox(height: 5),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Start:",
                          style: Theme.of(context).textTheme.caption,
                        ),
                        Text(
                          arrangedActivity.startAddress.streetAndNumber ?? "-",
                          style: Theme.of(context).textTheme.caption,
                        ),
                        Text(
                          (arrangedActivity.startAddress.postalCode ?? "-") +
                              " " +
                              (arrangedActivity.startAddress.city ?? "-"),
                          style: Theme.of(context).textTheme.caption,
                        )
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          "Ziel:",
                          style: Theme.of(context).textTheme.caption,
                        ),
                        Text(
                          arrangedActivity.address.streetAndNumber ?? "-",
                          style: Theme.of(context).textTheme.caption,
                        ),
                        Text(
                          (arrangedActivity.address.postalCode ?? "-") +
                              " " +
                              (arrangedActivity.address.city ?? "-"),
                          style: Theme.of(context).textTheme.caption,
                        )
                      ],
                    )
                  ],
                ),
              ],
            )),
      ),
    );
  }
}
