import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:planning_assistant/model/activity_plan.dart';
import 'package:planning_assistant/page/subpages/activity_plan_details_page.dart';

class ActivityPlanCard extends StatelessWidget {
  const ActivityPlanCard(
      {Key? key, required this.plan, this.deletePlan, this.savePlan})
      : super(key: key);

  final ActivityPlan plan;
  final Function? deletePlan;
  final Function? savePlan;

  void showDetails(context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ActivityPlanDetailPage(plan: plan),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.fromLTRB(16, 16, 16, 0),
      child: InkWell(
        onTap: () => showDetails(context),
        child: Padding(
          padding: EdgeInsets.all(12),
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    DateFormat('dd.MM.yyyy').format(plan.day),
                    style: Theme.of(context).textTheme.caption,
                  ),
                ],
              ),
              Text(plan.arrangedActivities.map((e) => e.title).join(', ')),
              if (deletePlan != null && savePlan != null)
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ElevatedButton.icon(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.red,
                      ),
                      onPressed: () => deletePlan!(),
                      icon: Icon(Icons.delete),
                      label: Text('Verwerfen'),
                    ),
                    ElevatedButton.icon(
                      onPressed: () => savePlan!(),
                      icon: Icon(Icons.save),
                      label: Text('Speichern'),
                    ),
                  ],
                ),
            ],
          ),
        ),
      ),
    );
  }
}
