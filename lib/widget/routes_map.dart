import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:planning_assistant/model/plan_route.dart';
import 'package:planning_assistant/services/utility.dart';

class RoutesMap extends StatelessWidget {
  const RoutesMap({Key? key, required this.routes}) : super(key: key);

  final List<PlanRoute> routes;

  LatLngBounds? getBounds(List<PlanRoute> routes) {
    var points = flatten(routes.map((r) => r.polyline.points))
        .map((p) => p.toLatLng())
        .toList();
    return LatLngBounds.fromPoints(points);
  }

  @override
  Widget build(BuildContext context) {
    return FlutterMap(
      options: MapOptions(
        bounds: getBounds(routes),
        boundsOptions: FitBoundsOptions(padding: EdgeInsets.all(10)),
      ),
      layers: [
        TileLayerOptions(
            urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            subdomains: ['a', 'b', 'c']),
        PolylineLayerOptions(
            polylines: routes
                .map(
                  (route) => Polyline(
                      points: route.polyline.toLatLongPolyline(),
                      strokeWidth: 4.0,
                      color: route.getRouteColor()),
                )
                .toList()),
        MarkerLayerOptions(
          markers: [
            ...routes
                .map((r) => [r.start.geoLocation, r.destination.geoLocation])
                .flattened
                .map(
                  (geoLocation) => Marker(
                    point: geoLocation!.toLatLng(),
                    builder: (ctx) => Container(
                      child: Icon(Icons.place),
                    ),
                  ),
                ),
          ],
        ),
      ],
    );
  }
}
