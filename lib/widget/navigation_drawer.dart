import 'package:flutter/material.dart';

class NavigationDrawer extends StatelessWidget {
  final padding = EdgeInsets.symmetric(horizontal: 20);

  void navigate(BuildContext context, String routeName) {
    Navigator.of(context).pop();
    if (ModalRoute.of(context)?.settings.name == routeName) return;

    Navigator.pushReplacementNamed(context, routeName);
  }

  Widget buildNavItem({
    required String text,
    required IconData icon,
    VoidCallback? onClicked,
  }) {
    return ListTile(
      leading: Icon(icon, color: Colors.white),
      title: Text(text, style: TextStyle(color: Colors.white)),
      hoverColor: Colors.white70,
      onTap: onClicked,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Material(
        color: Colors.green,
        child: ListView(
          children: <Widget>[
            buildNavItem(
              text: 'Tagesübersicht',
              icon: Icons.today_rounded,
              onClicked: () => navigate(context, '/overview'),
            ),
            buildNavItem(
              text: 'Aktivitätsplan Erstellen',
              icon: Icons.add_box_rounded,
              onClicked: () => navigate(context, '/newActivityPlan'),
            ),
            buildNavItem(
              text: 'Gespeicherte Aktivitätspläne',
              icon: Icons.table_rows_rounded,
              onClicked: () => navigate(context, '/activityPlans'),
            ),
            buildNavItem(
              text: 'Präfernzen verwalten',
              icon: Icons.favorite_rounded,
              onClicked: () => navigate(context, '/preferences'),
            ),
            buildNavItem(
              text: 'Einstellungen',
              icon: Icons.settings,
              onClicked: () => navigate(context, '/settings'),
            ),
          ],
        ),
      ),
    );
  }
}
