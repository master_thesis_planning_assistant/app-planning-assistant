import 'package:flutter/foundation.dart';
import 'package:planning_assistant/services/user_storage_service.dart';

class UserContext extends ChangeNotifier {
  late String _userName;
  late String _password;
  final UserStorageService storage = UserStorageService();
  late Future contextInitialized;

  UserContext() {
    _userName = "";
    _password = "";
    contextInitialized = initialize();
  }

  Future initialize() async {
    await storage.storageReady;
    _userName = await storage.getUserName();
    _password = await storage.getPassword();
    notifyListeners();
  }

  String get userName => _userName;

  String get password => _password;

  Future get initialized => contextInitialized;

  Future setPassword(String password) async {
    _password = password;
    notifyListeners();
    await storage.updatePassword(password);
  }

  Future setUserName(String userName) async {
    _userName = userName;
    notifyListeners();
    await storage.updateUserName(userName);
  }

  Future clearContext() async {
    _userName = "";
    _password = "";
    notifyListeners();
    await storage.clearStorage();
  }
}
