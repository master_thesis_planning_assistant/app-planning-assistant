import 'package:flutter/foundation.dart';
import 'package:planning_assistant/model/activity_plan.dart';
import 'package:planning_assistant/services/activity_plan_storage_service.dart';


class ActivityPlansContext extends ChangeNotifier {
  late List<ActivityPlan> _storedActivityPlans;
  final ActivityPlanStorageService storage = ActivityPlanStorageService();
  late Future contextInitialized;

  ActivityPlansContext() {
    _storedActivityPlans = [];
    contextInitialized = initialize();
  }

  Future initialize() async {
    await storage.storageReady;
    _storedActivityPlans = await storage.getAllActivityPlans();
    notifyListeners();
  }

  List<ActivityPlan> get activityPlans => _storedActivityPlans;

  Future add(ActivityPlan activityPlan) async {
    _storedActivityPlans.add(activityPlan);
    notifyListeners();
    await storage.saveActivityPlanToStorage(activityPlan);
  }

  Future remove(String guid) async {
    _storedActivityPlans.removeWhere((element) => element.guid == guid);
    notifyListeners();
    await storage.removeActivityPlanFromStorage(guid);
  }

  Future removeAll() async {
    _storedActivityPlans = [];
    notifyListeners();
    await storage.clearStorage();
  }
}
