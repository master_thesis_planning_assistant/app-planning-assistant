import 'package:json_annotation/json_annotation.dart';
import 'package:planning_assistant/model/geo_location.dart';

part 'address.g.dart';

@JsonSerializable(explicitToJson: true)
class Address {
  String? streetAndNumber;
  String? postalCode;
  String? city;
  GeoLocation? geoLocation;

  Address({
    required this.streetAndNumber,
    required this.postalCode,
    required this.city,
    this.geoLocation,
  });

  factory Address.fromJson(Map<String, dynamic> json) =>
      _$AddressFromJson(json);

  Map<String, dynamic> toJson() => _$AddressToJson(this);
}
