import 'package:json_annotation/json_annotation.dart';

part 'connection_preferences.g.dart';

@JsonSerializable(explicitToJson: true)
class ConnectionPreferences {
  int maxConnectingTime;
  int maxNumOfChanges;
  int minConnectingTime;
  bool minimizeChanges;

  ConnectionPreferences({
    required this.maxConnectingTime,
    required this.maxNumOfChanges,
    required this.minConnectingTime,
    required this.minimizeChanges,
  });

  factory ConnectionPreferences.fromJson(Map<String, dynamic> json) =>
      _$ConnectionPreferencesFromJson(json);

  Map<String, dynamic> toJson() => _$ConnectionPreferencesToJson(this);
}
