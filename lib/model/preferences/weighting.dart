import 'package:json_annotation/json_annotation.dart';

part 'weighting.g.dart';

@JsonSerializable(explicitToJson: true)
class Weighting {
  double comfort;
  double duration;
  double environment;
  double price;

  Weighting({
    required this.comfort,
    required this.duration,
    required this.environment,
    required this.price,
  });

  factory Weighting.fromJson(Map<String, dynamic> json) =>
      _$WeightingFromJson(json);

  Map<String, dynamic> toJson() => _$WeightingToJson(this);
}
