import 'package:json_annotation/json_annotation.dart';

part 'geofence.g.dart';

@JsonSerializable(explicitToJson: true)
class Geofence {
  double lat;
  double lon;
  double radius;

  Geofence({
    required this.lat,
    required this.lon,
    required this.radius,
  });

  factory Geofence.fromJson(Map<String, dynamic> json) =>
      _$GeofenceFromJson(json);

  Map<String, dynamic> toJson() => _$GeofenceToJson(this);
}
