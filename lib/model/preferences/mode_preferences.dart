import 'package:json_annotation/json_annotation.dart';
import 'package:planning_assistant/model/routing_mode.dart';

part 'mode_preferences.g.dart';

@JsonSerializable(explicitToJson: true)
class ModePreferences {
  List<RoutingMode> excludedModes;
  List<RoutingMode> neutralModes;
  List<RoutingMode> preferredModes;

  ModePreferences({
    required this.excludedModes,
    required this.neutralModes,
    required this.preferredModes,
  });

  factory ModePreferences.fromJson(Map<String, dynamic> json) =>
      _$ModePreferencesFromJson(json);

  Map<String, dynamic> toJson() => _$ModePreferencesToJson(this);
}
