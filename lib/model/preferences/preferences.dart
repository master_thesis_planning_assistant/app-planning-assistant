import 'package:json_annotation/json_annotation.dart';
import 'package:planning_assistant/model/preferences/connection_preferences.dart';
import 'package:planning_assistant/model/preferences/geofence.dart';
import 'package:planning_assistant/model/preferences/mode_preferences.dart';
import 'package:planning_assistant/model/preferences/weighting.dart';

part 'preferences.g.dart';

@JsonSerializable(explicitToJson: true)
class Preferences {
  String profileName;
  ConnectionPreferences connectionPreferences;
  int cyclingPace;
  List<String>? demandedComfortFactors;
  Geofence? geofence;
  int levelOfIntermodality;
  int luggageSize;
  double maxCyclingDistance;
  double maxWalkingDistance;
  ModePreferences modePreferences;
  bool noCyclingInBadWeather;
  List<String>? timeframe;
  int walkingPace;
  Weighting weighting;

  Preferences({
    required this.profileName,
    required this.connectionPreferences,
    required this.cyclingPace,
    required this.demandedComfortFactors,
    required this.geofence,
    required this.levelOfIntermodality,
    required this.luggageSize,
    required this.maxCyclingDistance,
    required this.maxWalkingDistance,
    required this.modePreferences,
    required this.noCyclingInBadWeather,
    required this.timeframe,
    required this.walkingPace,
    required this.weighting,
  });

  factory Preferences.fromJson(Map<String, dynamic> json) =>
      _$PreferencesFromJson(json);

  Map<String, dynamic> toJson() => _$PreferencesToJson(this);
}
