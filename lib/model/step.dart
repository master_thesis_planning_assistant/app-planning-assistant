import 'package:json_annotation/json_annotation.dart';
import 'package:planning_assistant/model/geo_polyline.dart';
import 'package:planning_assistant/model/named_geolocation.dart';

part 'step.g.dart';

@JsonSerializable(explicitToJson: true)
class PlanStep {
  GeoPolyline? polyline;
  NamedGeoLocation? startLocation;
  NamedGeoLocation? endLocation;
  double duration;
  double distance;
  String? action;

  PlanStep({
    required this.polyline,
    required this.startLocation,
    required this.endLocation,
    required this.duration,
    required this.distance,
    required this.action,
  });

  factory PlanStep.fromJson(Map<String, dynamic> json) =>
      _$PlanStepFromJson(json);

  Map<String, dynamic> toJson() => _$PlanStepToJson(this);
}
