import 'package:json_annotation/json_annotation.dart';
import 'package:planning_assistant/model/address.dart';

part 'activity.g.dart';

@JsonSerializable(explicitToJson: true)
class Activity {
  String guid;
  String title;
  Address? address;
  DateTime? startTime;
  double duration;
  bool fix;

  Activity({
    required this.guid,
    required this.title,
    this.address,
    this.startTime,
    required this.duration,
    required this.fix,
  });

  factory Activity.fromJson(Map<String, dynamic> json) =>
      _$ActivityFromJson(json);

  Map<String, dynamic> toJson() => _$ActivityToJson(this);
}
