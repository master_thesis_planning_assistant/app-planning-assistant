import 'package:json_annotation/json_annotation.dart';
import 'package:planning_assistant/model/address.dart';
import 'package:planning_assistant/model/plan_route.dart';

part 'arranged_activity.g.dart';

@JsonSerializable(explicitToJson: true)
class ArrangedActivity {
  String guid;
  String originalActivityGuid;
  String title;
  Address address;
  DateTime startTime;
  double duration;
  Address startAddress;
  PlanRoute route;
  List<PlanRoute> alternativeRoutes;

  ArrangedActivity({
    required this.guid,
    required this.originalActivityGuid,
    required this.title,
    required this.address,
    required this.startTime,
    required this.duration,
    required this.startAddress,
    required this.route,
    required this.alternativeRoutes,
  });

  factory ArrangedActivity.fromJson(Map<String, dynamic> json) =>
      _$ArrangedActivityFromJson(json);

  Map<String, dynamic> toJson() => _$ArrangedActivityToJson(this);
}
