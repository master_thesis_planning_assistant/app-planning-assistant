import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:planning_assistant/model/address.dart';
import 'package:planning_assistant/model/bounds.dart';
import 'package:planning_assistant/model/geo_polyline.dart';
import 'package:planning_assistant/model/leg.dart';
import 'package:planning_assistant/services/utility.dart';
import 'package:uuid/uuid.dart';

part 'plan_route.g.dart';

@JsonSerializable(explicitToJson: true)
class PlanRoute {
  String guid;
  String serviceName;
  List<Leg> legs;
  GeoPolyline polyline;
  Address start;
  DateTime departure;
  Address destination;
  DateTime arrival;
  double duration;
  double distance;
  Bounds bounds;
  double costs;

  PlanRoute({
    required this.guid,
    required this.serviceName,
    required this.legs,
    required this.polyline,
    required this.start,
    required this.departure,
    required this.destination,
    required this.arrival,
    required this.bounds,
    required this.duration,
    required this.distance,
    required this.costs,
  });

  // Convert the guid of a route to a color in a deterministic way
  // ==> same route always gets same color
  Color getRouteColor() {
    List<int> bytes = Uuid.parse(this.guid);
    var long = ByteData.view(Uint8List.fromList(bytes).buffer)
        .getUint64(0, Endian.host);
    return getColorByLong(long);
  }

  factory PlanRoute.fromJson(Map<String, dynamic> json) =>
      _$PlanRouteFromJson(json);

  Map<String, dynamic> toJson() => _$PlanRouteToJson(this);
}
