import 'package:json_annotation/json_annotation.dart';
import 'package:latlong2/latlong.dart';

part 'geo_location.g.dart';

@JsonSerializable(explicitToJson: true)
class GeoLocation {
  double latitude;
  double longitude;

  GeoLocation({
    required this.latitude,
    required this.longitude,
  });

  factory GeoLocation.fromJson(Map<String, dynamic> json) =>
      _$GeoLocationFromJson(json);

  Map<String, dynamic> toJson() => _$GeoLocationToJson(this);

  LatLng toLatLng() {
    return LatLng(latitude, longitude);
  }
}
