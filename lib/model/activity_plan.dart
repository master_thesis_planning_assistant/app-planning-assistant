import 'package:json_annotation/json_annotation.dart';
import 'package:planning_assistant/model/arranged_activity.dart';
import 'package:planning_assistant/model/user.dart';

part 'activity_plan.g.dart';

@JsonSerializable(explicitToJson: true)
class ActivityPlan {
  String guid;
  DateTime day;
  User user;
  List<ArrangedActivity> arrangedActivities;

  ActivityPlan({
    required this.guid,
    required this.day,
    required this.arrangedActivities,
    required this.user,
  });

  factory ActivityPlan.fromJson(Map<String, dynamic> json) =>
      _$ActivityPlanFromJson(json);

  Map<String, dynamic> toJson() => _$ActivityPlanToJson(this);
}
