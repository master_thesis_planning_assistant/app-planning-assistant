import 'package:json_annotation/json_annotation.dart';
import 'package:latlong2/latlong.dart';
import 'package:planning_assistant/model/geo_location.dart';

part 'geo_polyline.g.dart';

@JsonSerializable(explicitToJson: true)
class GeoPolyline {
  List<GeoLocation> points;

  GeoPolyline({required this.points});

  GeoPolyline getPart(int startIndex, int endIndex) {
    return GeoPolyline(points: points.sublist(startIndex, endIndex));
  }

  List<LatLng> toLatLongPolyline() {
    return points.map((p) => p.toLatLng()).toList();
  }

  factory GeoPolyline.fromJson(Map<String, dynamic> json) =>
      _$GeoPolylineFromJson(json);

  Map<String, dynamic> toJson() => _$GeoPolylineToJson(this);
}
