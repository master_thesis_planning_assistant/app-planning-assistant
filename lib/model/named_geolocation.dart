import 'package:json_annotation/json_annotation.dart';
import 'package:planning_assistant/model/geo_location.dart';

part 'named_geolocation.g.dart';

@JsonSerializable(explicitToJson: true)
class NamedGeoLocation {
  String? name;
  GeoLocation? geoLocation;

  NamedGeoLocation({
    required this.name,
    required this.geoLocation,
  });

  factory NamedGeoLocation.fromJson(Map<String, dynamic> json) =>
      _$NamedGeoLocationFromJson(json);

  Map<String, dynamic> toJson() => _$NamedGeoLocationToJson(this);
}
