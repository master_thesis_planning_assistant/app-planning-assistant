import 'package:json_annotation/json_annotation.dart';
import 'package:planning_assistant/model/address.dart';
import 'package:planning_assistant/model/preferences/preferences.dart';

part 'user.g.dart';

@JsonSerializable(explicitToJson: true)
class User {
  String name;
  String password;
  Address homeAddress;
  List<Preferences> preferences;

  User({
    required this.name,
    required this.password,
    required this.homeAddress,
    required this.preferences,
  });

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
