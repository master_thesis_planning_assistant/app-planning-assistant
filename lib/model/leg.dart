import 'package:json_annotation/json_annotation.dart';
import 'package:planning_assistant/model/geo_polyline.dart';
import 'package:planning_assistant/model/named_geolocation.dart';
import 'package:planning_assistant/model/routing_mode.dart';
import 'package:planning_assistant/model/step.dart';

part 'leg.g.dart';

@JsonSerializable(explicitToJson: true)
class Leg {
  RoutingMode modality;
  String? description;
  GeoPolyline polyline;
  NamedGeoLocation startLocation;
  DateTime departure;
  NamedGeoLocation endLocation;
  DateTime arrival;
  double duration;
  double distance;
  List<PlanStep> steps;

  Leg({
    required this.modality,
    required this.description,
    required this.polyline,
    required this.startLocation,
    required this.departure,
    required this.endLocation,
    required this.arrival,
    required this.duration,
    required this.distance,
    required this.steps,
  });

  factory Leg.fromJson(Map<String, dynamic> json) => _$LegFromJson(json);

  Map<String, dynamic> toJson() => _$LegToJson(this);
}
