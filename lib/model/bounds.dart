import 'package:json_annotation/json_annotation.dart';
import 'package:planning_assistant/model/geo_location.dart';

part 'bounds.g.dart';

@JsonSerializable(explicitToJson: true)
class Bounds {
  GeoLocation southwest;
  GeoLocation northeast;

  Bounds({
    required this.southwest,
    required this.northeast,
  });

  factory Bounds.fromJson(Map<String, dynamic> json) => _$BoundsFromJson(json);

  Map<String, dynamic> toJson() => _$BoundsToJson(this);
}
