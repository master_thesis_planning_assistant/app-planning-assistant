import 'package:json_annotation/json_annotation.dart';

enum RoutingMode {
  @JsonValue(0)
  Walk,
  @JsonValue(1)
  BikeSharing,
  @JsonValue(2)
  Bike,
  @JsonValue(3)
  PublicTransport,
  @JsonValue(4)
  Car,
  @JsonValue(5)
  CarSharing
}
