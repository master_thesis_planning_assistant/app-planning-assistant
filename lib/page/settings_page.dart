import 'package:flutter/material.dart';
import 'package:planning_assistant/context/activity_plans_context.dart';
import 'package:planning_assistant/context/user_context.dart';
import 'package:planning_assistant/widget/navigation_drawer.dart';
import 'package:provider/provider.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  var editMode = false;

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    var activityPlanContext = context.watch<ActivityPlansContext>();
    var userContext = context.watch<UserContext>();

    onSave() {
      if (_formKey.currentState!.validate()) {
        _formKey.currentState!.save();
        setState(() {
          editMode = false;
        });
      }
    }

    onEdit() {
      setState(() {
        editMode = true;
      });
    }

    onCancel() {
      setState(() {
        editMode = false;
      });
    }

    return Scaffold(
      drawer: NavigationDrawer(),
      appBar: AppBar(
        title: Text('Einstellungen'),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: [
            Text("Nutzer", style: Theme.of(context).textTheme.headline6),
            if (!editMode)
              Column(
                children: [
                  Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(Icons.person),
                          Text(userContext.userName),
                        ],
                      )),
                  Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(Icons.password),
                          Text(userContext.password),
                        ],
                      )),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ElevatedButton.icon(
                        onPressed: () => onEdit(),
                        icon: Icon(Icons.edit),
                        label: Text('Bearbeiten'),
                      ),
                    ],
                  ),
                ],
              ),
            if (editMode)
              Column(
                children: [
                  Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            initialValue: userContext.userName,
                            decoration: InputDecoration(
                              icon: Icon(Icons.person),
                              labelText: 'Nutzername',
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Nutzername ist ein Pflichtfeldd';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              userContext.setUserName(value!);
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            initialValue: userContext.password,
                            decoration: InputDecoration(
                              icon: Icon(Icons.password),
                              labelText: 'Password',
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Password ist ein Pflichtfeld';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              userContext.setPassword(value!);
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ElevatedButton.icon(
                        onPressed: () => onCancel(),
                        icon: Icon(Icons.cancel),
                        label: Text('Abbrechen'),
                      ),
                      ElevatedButton.icon(
                        onPressed: () => onSave(),
                        icon: Icon(Icons.save),
                        label: Text('Speichern'),
                      ),
                    ],
                  ),
                ],
              ),
            Divider(),
            Text("Lokale Daten", style: Theme.of(context).textTheme.headline6),
            ElevatedButton.icon(
              onPressed: () => userContext.clearContext(),
              icon: Icon(Icons.delete_forever),
              label: Text('Gespeicherten Nutzer Löschen'),
            ),
            ElevatedButton.icon(
              onPressed: () => activityPlanContext.removeAll(),
              icon: Icon(Icons.delete_forever),
              label: Text('Gespeicherte Aktivitätspläne Löschen'),
            )
          ],
        ),
      ),
    );
  }
}
