import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:planning_assistant/context/user_context.dart';
import 'package:planning_assistant/page/subpages/preference_profile_details_page.dart';
import 'package:planning_assistant/services/backend/backend_service.dart';
import 'package:planning_assistant/services/backend/model/user_preference_response.dart';
import 'package:planning_assistant/widget/navigation_drawer.dart';
import 'package:provider/provider.dart';

class PreferencesPage extends StatefulWidget {
  const PreferencesPage({Key? key}) : super(key: key);

  @override
  _PreferencesPageState createState() => _PreferencesPageState();
}

class _PreferencesPageState extends State<PreferencesPage> {
  Widget displayPreference(String title, String value) {
    return Row(
      children: [
        Text(
          title + ': ',
          style: Theme.of(context).textTheme.bodyText1,
        ),
        Text(
          value,
          style: Theme.of(context).textTheme.bodyText2,
        ),
      ],
    );
  }

  void showDetails(context, preferences) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) =>
            PreferenceProfileDetailsPage(preferences: preferences),
      ),
    );
  }

  Future<UserPreferenceResponse> loadUserPreferences(
      UserContext userContext) async {
    await userContext.initialized;
    return await getUserPreferences(userContext.userName, userContext.password);
  }

  @override
  Widget build(BuildContext context) {
    var userContext = context.watch<UserContext>();
    return Scaffold(
      drawer: NavigationDrawer(),
      appBar: AppBar(
        title: Text('Präferenzen'),
        centerTitle: true,
      ),
      body: FutureBuilder<UserPreferenceResponse>(
        future: loadUserPreferences(userContext),
        builder: (BuildContext context,
            AsyncSnapshot<UserPreferenceResponse> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: const CircularProgressIndicator());
          } else {
            if (snapshot.hasError)
              return Center(child: Text('Fehler: ${snapshot.error}'));
            else
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListView(
                  children: [
                    Text(
                      'Heimatadresse:',
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            displayPreference(
                                "Straße und Nr.",
                                snapshot.data!.homeAddress.streetAndNumber ??
                                    "-"),
                            displayPreference("Postleitzahl",
                                snapshot.data!.homeAddress.postalCode ?? "-"),
                            displayPreference("Stadt",
                                snapshot.data!.homeAddress.city ?? "-"),
                          ],
                        )),
                    Text(
                      'Präferenzprofile:',
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: snapshot.data!.preferences
                            .map(
                              (preference) => Card(
                                margin: EdgeInsets.fromLTRB(0, 8, 8, 0),
                                child: InkWell(
                                  onTap: () => showDetails(context, preference),
                                  child: Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: Row(
                                      children: [
                                        Text(
                                          preference.profileName,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            )
                            .toList(),
                      ),
                    ),
                  ],
                ),
              );
          }
        },
      ),
    );
  }
}
