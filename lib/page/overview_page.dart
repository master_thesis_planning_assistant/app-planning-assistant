import 'package:flutter/material.dart';
import 'package:planning_assistant/context/activity_plans_context.dart';
import 'package:planning_assistant/model/activity_plan.dart';
import 'package:planning_assistant/page/subpages/route_map_page.dart';
import 'package:planning_assistant/services/utility.dart';
import 'package:planning_assistant/widget/arranged_activity_card.dart';
import 'package:planning_assistant/widget/navigation_drawer.dart';
import 'package:provider/provider.dart';

class OverviewPage extends StatefulWidget {
  const OverviewPage({Key? key}) : super(key: key);

  @override
  _OverviewPageState createState() => _OverviewPageState();
}

class _OverviewPageState extends State<OverviewPage> {
  void showOnMap(context, ActivityPlan plan) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => RouteMapPage(
          routes: plan.arrangedActivities.map((a) => a.route).toList(),
          title: "Alle Aktivitäten",
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var activityPlanContext = context.watch<ActivityPlansContext>();
    var plansForToday = activityPlanContext.activityPlans
        .where((plan) => getDate(plan.day) == getDate(DateTime.now()))
        .toList();

    if (plansForToday.length < 1 || plansForToday.isEmpty) {
      return Scaffold(
        drawer: NavigationDrawer(),
        appBar: AppBar(
          title: Text('Tagesübersicht'),
          centerTitle: true,
        ),
        body: Center(
          child: Text("Für heute sind noch keine Aktivitätspläne hinterlegt."),
        ),
      );
    }

    if (plansForToday.length > 1) {
      return Scaffold(
        drawer: NavigationDrawer(),
        appBar: AppBar(
          title: Text('Tagesübersicht'),
          centerTitle: true,
        ),
        body: Center(
          child: Text("Für heute sind mehrere Aktivitätspläne hinterlegt."),
        ),
      );
    }

    var activityPlan = plansForToday[0];

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () => showOnMap(context, activityPlan),
        child: Icon(Icons.map),
      ),
      drawer: NavigationDrawer(),
      appBar: AppBar(
        title: Text('Tagesübersicht'),
        centerTitle: true,
      ),
      body: ListView(
        children: [
          Column(
            children: [
              ...activityPlan.arrangedActivities
                  .map((arrangedActivity) =>
                      ArrangedActivityCard(arrangedActivity: arrangedActivity))
                  .toList(),
            ],
          ),
        ],
      ),
    );
  }
}
