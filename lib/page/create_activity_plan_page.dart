import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:planning_assistant/context/user_context.dart';
import 'package:planning_assistant/model/activity.dart';
import 'package:planning_assistant/page/subpages/activity_plan_review_page.dart';
import 'package:planning_assistant/page/subpages/create_activity_page.dart';
import 'package:planning_assistant/services/utility.dart';
import 'package:planning_assistant/widget/activity_card.dart';
import 'package:planning_assistant/widget/navigation_drawer.dart';
import 'package:provider/provider.dart';

class CreateActivityPlanPage extends StatefulWidget {
  const CreateActivityPlanPage({Key? key}) : super(key: key);

  @override
  _CreateActivityPlanPageState createState() => _CreateActivityPlanPageState();
}

class _CreateActivityPlanPageState extends State<CreateActivityPlanPage> {
  final List<Activity> activities = [];
  DateTime date = getDate(DateTime.now());
  bool filterDuplicates = true;

  void handleDateFilter() async {
    var result = await showDatePicker(
      context: context,
      initialDate: date,
      cancelText: 'Abbrechen',
      helpText: 'Tag des Aktivitätsplans auswählen',
      confirmText: 'Ok',
      firstDate: DateTime.now(),
      lastDate: DateTime(2100),
    );
    if (result != null)
      setState(() {
        date = result;
      });
  }

  void onCreate(BuildContext context) async {
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => CreateActivityPage(),
        ));
    if (result == null) return;
    if (result.startTime != null) {
      result.startTime = new DateTime(date.year, date.month, date.day,
          result.startTime.hour, result.startTime.minute);
    } else {
      result.startTime = new DateTime(
        date.year,
        date.month,
        date.day,
      );
    }
    setState(() {
      activities.add(result);
    });
  }

  void onImport() {}

  void onDelete(String guid) {
    setState(() {
      activities.removeWhere((element) => element.guid == guid);
    });
  }

  void onCalculateActivityPlan(
      context, String userName, String password) async {
    // Make sure all activities are set on the right day
    activities.forEach((a) {
      a.startTime = new DateTime(date.year, date.month, date.day,
          a.startTime!.hour, a.startTime!.minute);
    });

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ActivityPlanReviewPage(
          userName: userName,
          password: password,
          activities: activities,
          day: date,
          filterDuplicates: filterDuplicates,
        ),
      ),
    );
  }

  void onChangeFilterDuplicates(bool? value) {
    if (value == null) return;
    setState(() {
      filterDuplicates = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    var userContext = context.watch<UserContext>();
    return Scaffold(
      drawer: NavigationDrawer(),
      appBar: AppBar(
        title: Text('Aktivitätsplan Erstellen'),
        centerTitle: true,
      ),
      body: Stack(
        alignment: AlignmentDirectional.bottomCenter,
        children: [
          Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton.icon(
                    onPressed: handleDateFilter,
                    icon: Icon(Icons.today),
                    label: Text(DateFormat('dd.MM.yyyy').format(date)),
                  ),
                  Row(
                    children: [
                      Checkbox(
                        value: filterDuplicates,
                        onChanged: onChangeFilterDuplicates,
                      ),
                      Text(
                        "Routenduplikate filtern",
                        style: Theme.of(context).textTheme.button,
                      )
                    ],
                  )
                ],
              ),
              Expanded(
                child: ListView(
                  children: [
                    ...activities
                        .map(
                          (activity) => ActivityCard(
                            activity: activity,
                            delete: () => onDelete(activity.guid),
                          ),
                        )
                        .toList(),
                    SizedBox(height: 100),
                  ],
                ),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                FloatingActionButton(
                  heroTag: null,
                  backgroundColor: activities.length > 0 ? null : Colors.grey,
                  onPressed: activities.length > 0
                      ? () => onCalculateActivityPlan(
                          context, userContext.userName, userContext.password)
                      : null,
                  child: Icon(Icons.check),
                ),
                FloatingActionButton(
                  heroTag: null,
                  onPressed: onImport,
                  child: Icon(Icons.calendar_today_rounded),
                ),
                FloatingActionButton(
                  heroTag: null,
                  onPressed: () => onCreate(context),
                  child: Icon(Icons.add),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
