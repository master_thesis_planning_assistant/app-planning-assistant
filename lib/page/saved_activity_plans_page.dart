import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:planning_assistant/context/activity_plans_context.dart';
import 'package:planning_assistant/services/utility.dart';
import 'package:planning_assistant/widget/activity_plan_card.dart';
import 'package:planning_assistant/widget/navigation_drawer.dart';
import 'package:provider/provider.dart';

class SavedActivityPlansPage extends StatefulWidget {
  const SavedActivityPlansPage({Key? key}) : super(key: key);

  @override
  _SavedActivityPlansPageState createState() => _SavedActivityPlansPageState();
}

class _SavedActivityPlansPageState extends State<SavedActivityPlansPage> {
  DateTime dateFilter = getDate(DateTime.now());

  bool showAll = true;

  void handleDateFilter() async {
    var result = await showDatePicker(
      context: context,
      initialDate: dateFilter,
      firstDate: new DateTime(2000, 01, 01),
      lastDate: DateTime.now(),
    );
    if (result != null)
      setState(() {
        dateFilter = result;
        showAll = false;
      });
  }

  void handleShowAll() {
    setState(() {
      showAll = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    var activityPlanContext = context.watch<ActivityPlansContext>();

    var filteredPlans = showAll
        ? activityPlanContext.activityPlans
        : activityPlanContext.activityPlans
            .where((plan) => getDate(plan.day) == dateFilter);

    return Scaffold(
      drawer: NavigationDrawer(),
      appBar: AppBar(
        title: Text('Gespeicherte Aktivitätspläne'),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ElevatedButton.icon(
                icon: Icon(Icons.format_align_left),
                label: Text("Alle anzeigen"),
                onPressed: handleShowAll,
                style: ElevatedButton.styleFrom(
                    primary:
                        showAll ? Theme.of(context).primaryColor : Colors.grey),
              ),
              ElevatedButton.icon(
                icon: Icon(Icons.filter_alt_rounded),
                label: Text(DateFormat('dd.MM.yyyy').format(dateFilter)),
                onPressed: handleDateFilter,
                style: ElevatedButton.styleFrom(
                    primary: !showAll
                        ? Theme.of(context).primaryColor
                        : Colors.grey),
              ),
            ],
          ),
          Expanded(
            child: ListView(
              children: filteredPlans.map((plan) {
                return ActivityPlanCard(
                  plan: plan,
                );
              }).toList(),
            ),
          ),
        ],
      ),
    );
  }
}
