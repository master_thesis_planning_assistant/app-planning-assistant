import 'package:flutter/material.dart';
import 'package:planning_assistant/model/preferences/preferences.dart';
import 'package:planning_assistant/services/utility.dart';

class PreferenceProfileDetailsPage extends StatelessWidget {
  const PreferenceProfileDetailsPage({Key? key, required this.preferences})
      : super(key: key);

  final Preferences preferences;

  @override
  Widget build(BuildContext context) {
    Widget displayPreference(String title, String value) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(0, 2, 0, 2),
        child: Row(
          children: [
            Text(
              title + ': ',
              style: Theme.of(context).textTheme.bodyText1,
              softWrap: true,
              maxLines: 2,
            ),
            Flexible(
              child: Text(
                value,
                style: Theme.of(context).textTheme.bodyText2,
                softWrap: true,
                maxLines: 2,
              ),
            ),
          ],
        ),
      );
    }

    return Scaffold(
        appBar: AppBar(
          title: Text(preferences.profileName),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: ListView(
            children: [
              displayPreference("Verbindungspräferenzen", ""),
              Padding(
                padding: const EdgeInsets.fromLTRB(16, 0, 0, 0),
                child: Column(
                  children: [
                    displayPreference(
                        "Maximale Verbindungsdauer",
                        preferences.connectionPreferences.maxConnectingTime
                            .toString()),
                    displayPreference(
                        "Maximale Anzahl Umstiege",
                        preferences.connectionPreferences.maxNumOfChanges
                            .toString()),
                    displayPreference(
                        "Minimale Verbindungsdauer",
                        preferences.connectionPreferences.minConnectingTime
                            .toString()),
                    displayPreference(
                        "Umstiege minimieren",
                        preferences.connectionPreferences.minimizeChanges
                            ? "Ja"
                            : "Nein"),
                  ],
                ),
              ),
              displayPreference("Fahrrad-Geschwindigkeit",
                  preferences.cyclingPace.toString()),
              displayPreference(
                  "Gewünschte Komfortfaktoren",
                  preferences.demandedComfortFactors != null
                      ? preferences.demandedComfortFactors!.join(', ')
                      : "-"),
              displayPreference("Geo-Zaun", ""),
              Padding(
                padding: const EdgeInsets.fromLTRB(16, 0, 0, 0),
                child: Column(
                  children: [
                    displayPreference(
                        "Breite",
                        preferences.geofence != null
                            ? preferences.geofence!.lat.toString()
                            : "-"),
                    displayPreference(
                        "Länge",
                        preferences.geofence != null
                            ? preferences.geofence!.lon.toString()
                            : "-"),
                    displayPreference(
                        "Radius",
                        preferences.geofence != null
                            ? preferences.geofence!.radius.toString()
                            : "-"),
                  ],
                ),
              ),
              displayPreference("Grad an Intermodalität",
                  preferences.levelOfIntermodality.toString()),
              displayPreference(
                  "Gepäckgröße", preferences.luggageSize.toString()),
              displayPreference("Maximale Fahrraddistanz",
                  preferences.maxCyclingDistance.toString()),
              displayPreference("Maximale Laufdistanz",
                  preferences.maxWalkingDistance.toString()),
              displayPreference("Moduspräferenzen", ""),
              Padding(
                padding: const EdgeInsets.fromLTRB(16, 0, 0, 0),
                child: Column(
                  children: [
                    displayPreference(
                        "Ausgeschlossene Modi",
                        preferences.modePreferences.excludedModes
                            .map((m) => getRoutingModeString(m))
                            .join(', ')),
                    displayPreference(
                        "Neutrale Modi",
                        preferences.modePreferences.neutralModes
                            .map((m) => getRoutingModeString(m))
                            .join(', ')),
                    displayPreference(
                        "Bevorzugte Modi",
                        preferences.modePreferences.preferredModes
                            .map((m) => getRoutingModeString(m))
                            .join(', ')),
                  ],
                ),
              ),
              displayPreference("Kein Fahrrad bei schlechtem Wetter",
                  preferences.noCyclingInBadWeather ? "Ja" : "Nein"),
              displayPreference(
                  "Zeitrahmen",
                  preferences.timeframe != null
                      ? preferences.timeframe!.join(', ')
                      : '-'),
              displayPreference(
                  "Laufgeschwindigkeit", preferences.walkingPace.toString()),
              displayPreference("Gewichtung", ""),
              Padding(
                padding: const EdgeInsets.fromLTRB(16, 0, 0, 0),
                child: Column(
                  children: [
                    displayPreference(
                        "Komfort", preferences.weighting.comfort.toString()),
                    displayPreference(
                        "Dauer", preferences.weighting.duration.toString()),
                    displayPreference(
                        "Umwelt", preferences.weighting.environment.toString()),
                    displayPreference(
                        "Preis", preferences.weighting.price.toString()),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
