import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:planning_assistant/model/arranged_activity.dart';
import 'package:planning_assistant/page/subpages/route_map_page.dart';
import 'package:planning_assistant/widget/route_card.dart';

class ArrangedActivityDetails extends StatelessWidget {
  const ArrangedActivityDetails({Key? key, required this.arrangedActivity})
      : super(key: key);

  final ArrangedActivity arrangedActivity;

  void showOnMap(context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => RouteMapPage(
          routes: [
            arrangedActivity.route,
            ...arrangedActivity.alternativeRoutes
          ],
          title: arrangedActivity.title,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget displayActivityProperty(String key, String value) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            key,
            style: Theme.of(context).textTheme.bodyText1,
          ),
          Text(
            value,
            style: Theme.of(context).textTheme.caption,
          ),
        ],
      );
    }

    arrangedActivity.alternativeRoutes
        .sort((a, b) => a.departure.compareTo(b.departure));

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () => showOnMap(context),
        child: Icon(Icons.map),
      ),
      appBar: AppBar(
        title: Text(arrangedActivity.title),
      ),
      body: ListView(
        children: [
          Card(
            margin: EdgeInsets.fromLTRB(16, 16, 16, 8),
            child: Padding(
              padding: EdgeInsets.all(12),
              child: Column(
                children: [
                  displayActivityProperty("Titel:", arrangedActivity.title),
                  displayActivityProperty(
                      "Startzeit:",
                      DateFormat('HH:mm').format(arrangedActivity.startTime) +
                          " Uhr"),
                  displayActivityProperty(
                      "Ziel:",
                      (arrangedActivity.address.streetAndNumber ?? "-") +
                          " " +
                          (arrangedActivity.address.postalCode ?? "-") +
                          " " +
                          (arrangedActivity.address.city ?? "-")),
                  displayActivityProperty(
                      "Start:",
                      (arrangedActivity.startAddress.streetAndNumber ?? "-") +
                          " " +
                          (arrangedActivity.startAddress.postalCode ?? "-") +
                          " " +
                          (arrangedActivity.startAddress.city ?? "-")),
                  displayActivityProperty(
                      "Dauer:",
                      new Duration(seconds: arrangedActivity.duration.round())
                              .inMinutes
                              .toString() +
                          " min"),
                ],
              ),
            ),
          ),
          Divider(
            thickness: 1.3,
            indent: 50,
            endIndent: 50,
          ),
          RouteCard(
            route: arrangedActivity.route,
            title: "Route",
          ),
          Divider(
            thickness: 1.3,
            indent: 50,
            endIndent: 50,
          ),
          ...arrangedActivity.alternativeRoutes.mapIndexed((i, route) =>
              RouteCard(route: route, title: "Alternative " + i.toString())),
          SizedBox(height: 100),
        ],
      ),
    );
  }
}
