import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:planning_assistant/model/plan_route.dart';
import 'package:planning_assistant/services/utility.dart';
import 'package:planning_assistant/widget/routes_map.dart';

class RouteMapPage extends StatefulWidget {
  const RouteMapPage({Key? key, required this.routes, required this.title})
      : super(key: key);

  final List<PlanRoute> routes;
  final String title;

  @override
  _RouteMapPageState createState() => _RouteMapPageState();
}

class _RouteMapPageState extends State<RouteMapPage> {
  LatLngBounds? getBounds(List<PlanRoute> routes) {
    var points = flatten(routes.map((r) => r.polyline.points))
        .map((p) => p.toLatLng())
        .toList();
    return LatLngBounds.fromPoints(points);
  }

  var controller = ExpandableController(initialExpanded: false);

  void toggleExpanded() {
    setState(() {
      controller.toggle();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Stack(
        fit: StackFit.loose,
        children: [
          RoutesMap(routes: widget.routes),
          ExpandablePanel(
            controller: controller,
            theme: ExpandableThemeData(hasIcon: false),
            header: Card(
              child: InkWell(
                onTap: toggleExpanded,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(10, 2, 10, 2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Routen-Details",
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                      if (controller.expanded) Icon(Icons.expand_less),
                      if (!controller.expanded) Icon(Icons.expand_more)
                    ],
                  ),
                ),
              ),
            ),
            collapsed: SizedBox(),
            expanded: Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: widget.routes
                      .map(
                        (route) => Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(
                              height: 10,
                              width: 40,
                              child: Container(
                                color: route.getRouteColor(),
                              ),
                            ),
                            Text('Länge: ' +
                                (route.distance / 1000).toStringAsFixed(1) +
                                ' km'),
                            Text("Dauer: " +
                                new Duration(seconds: route.duration.round())
                                    .inMinutes
                                    .toString() +
                                " min"),
                          ],
                        ),
                      )
                      .toList(),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
