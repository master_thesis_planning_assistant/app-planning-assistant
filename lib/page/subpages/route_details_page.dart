import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:planning_assistant/model/plan_route.dart';
import 'package:planning_assistant/page/subpages/route_map_page.dart';
import 'package:planning_assistant/widget/leg_card.dart';

class RouteDetailPage extends StatelessWidget {
  const RouteDetailPage({Key? key, required this.route, required this.title})
      : super(key: key);

  final PlanRoute route;
  final String title;

  void showOnMap(context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => RouteMapPage(
          routes: [route],
          title: title,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget displayRouteProperty(String key, String value) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            key,
            style: Theme.of(context).textTheme.bodyText1,
          ),
          Text(
            value,
            style: Theme.of(context).textTheme.caption,
          ),
        ],
      );
    }

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () => showOnMap(context),
        child: Icon(Icons.map),
      ),
      appBar: AppBar(
        title: Text(title),
      ),
      body: ListView(
        children: [
          Card(
            margin: EdgeInsets.fromLTRB(16, 16, 16, 8),
            child: Padding(
              padding: EdgeInsets.all(12),
              child: Column(
                children: [
                  displayRouteProperty("Routing-Service", route.serviceName),
                  displayRouteProperty(
                      "Start:",
                      (route.start.streetAndNumber ?? "-") +
                          " " +
                          (route.start.postalCode ?? "-") +
                          " " +
                          (route.start.city ?? "-")),
                  displayRouteProperty(
                      "Ziel:",
                      (route.destination.streetAndNumber ?? "-") +
                          " " +
                          (route.destination.postalCode ?? "-") +
                          " " +
                          (route.destination.city ?? "-")),
                  displayRouteProperty(
                      "An:",
                      DateFormat('HH:mm').format(route.arrival.toLocal()) +
                          " Uhr"),
                  displayRouteProperty(
                      "Ab:",
                      DateFormat('HH:mm').format(route.arrival.toLocal()) +
                          " Uhr"),
                  displayRouteProperty(
                      "Dauer:",
                      new Duration(seconds: route.duration.round())
                              .inMinutes
                              .toString() +
                          " min"),
                  displayRouteProperty("Strecke: ",
                      (route.distance / 1000).toStringAsFixed(1) + " km"),
                ],
              ),
            ),
          ),
          Divider(
            thickness: 1.3,
            indent: 50,
            endIndent: 50,
          ),
          Center(
            child: Text(
              "Teilstrecken",
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          ...route.legs.map(
            (leg) => LegCard(leg: leg),
          ),
          SizedBox(height: 100),
        ],
      ),
    );
  }
}
