import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:planning_assistant/model/activity.dart';
import 'package:planning_assistant/model/address.dart';
import 'package:planning_assistant/widget/date_time_fields.dart';
import 'package:uuid/uuid.dart';

class CreateActivityPage extends StatefulWidget {
  const CreateActivityPage({Key? key}) : super(key: key);

  @override
  _CreateActivityPageState createState() => _CreateActivityPageState();
}

class _CreateActivityPageState extends State<CreateActivityPage> {
  final _formKey = GlobalKey<FormState>();
  final Activity activity = Activity(
    guid: Uuid().v4(),
    title: '',
    startTime: null,
    duration: 0,
    fix: true,
    address: null,
  );

  void onSave() {
    if (activity.startTime == null) {
      activity.fix = false;
    }
    Navigator.pop(context, activity);
  }

  void addAddress() {
    setState(() {
      activity.address = Address(
        streetAndNumber: '',
        postalCode: '',
        city: '',
      );
    });
  }

  void removeAddress() {
    setState(() {
      activity.address = null;
    });
  }

  void onCancel() {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: null,
      appBar: AppBar(
        title: Text('Neue Aktivität'),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
            onSave();
          }
        },
        child: Icon(Icons.save),
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                decoration: InputDecoration(
                  icon: Icon(Icons.label),
                  labelText: 'Titel*',
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Titel ist ein Pflichtfeld';
                  }
                  return null;
                },
                onSaved: (value) {
                  setState(() {
                    activity.title = value!;
                  });
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: BasicTimeField(
                text: 'Startzeitpunkt',
                onSave: (value) {
                  setState(() {
                    activity.startTime = value;
                  });
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                keyboardType: TextInputType.number,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                decoration: InputDecoration(
                    icon: Icon(Icons.timelapse),
                    labelText: 'Dauer in Minuten*'),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Dauer ist ein Pflichtfeld';
                  }
                  return null;
                },
                onSaved: (value) {
                  var number = double.parse(value!);
                  setState(() {
                    activity.duration = number * 60;
                  });
                },
              ),
            ),
            activity.address == null
                ? Column(
                    children: [
                      ElevatedButton.icon(
                        onPressed: addAddress,
                        icon: Icon(Icons.add),
                        label: Text('Adresse hinzufügen'),
                      ),
                    ],
                  )
                : Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
                        child: TextFormField(
                          decoration: InputDecoration(
                            icon: Icon(Icons.place),
                            labelText: 'Straße und Nr.*',
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Straße und Nr. ist ein Pflichtfeld';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            setState(() {
                              activity.address!.streetAndNumber = value!;
                            });
                          },
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
                        child: TextFormField(
                          decoration: InputDecoration(
                            icon: Icon(Icons.filter_7),
                            labelText: 'Plz*',
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Plz ist ein Pflichtfeld';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            setState(() {
                              activity.address!.postalCode = value!;
                            });
                          },
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
                        child: TextFormField(
                          decoration: InputDecoration(
                            icon: Icon(Icons.location_city),
                            labelText: 'Stadt*',
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Stadt ist ein Pflichtfeld';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            setState(() {
                              activity.address!.city = value!;
                            });
                          },
                        ),
                      ),
                      ElevatedButton.icon(
                        onPressed: removeAddress,
                        icon: Icon(Icons.remove),
                        label: Text('Adresse entfernen'),
                      ),
                    ],
                  ),
          ],
        ),
      ),
    );
  }
}
