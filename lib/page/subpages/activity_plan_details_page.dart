import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:planning_assistant/model/activity_plan.dart';
import 'package:planning_assistant/page/subpages/route_map_page.dart';
import 'package:planning_assistant/widget/arranged_activity_card.dart';

class ActivityPlanDetailPage extends StatelessWidget {
  const ActivityPlanDetailPage({Key? key, required this.plan})
      : super(key: key);

  final ActivityPlan plan;

  void showOnMap(context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => RouteMapPage(
          routes: plan.arrangedActivities.map((a) => a.route).toList(),
          title: "Alle Aktivitäten",
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    plan.arrangedActivities.sort((a, b) => a.startTime.compareTo(b.startTime));

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () => showOnMap(context),
        child: Icon(Icons.map),
      ),
      appBar: AppBar(
        title:
            Text('Aktivitätsplan ' + DateFormat('dd.MM.yyyy').format(plan.day)),
      ),
      body: ListView(
        children: plan.arrangedActivities
            .map(
              (activity) => ArrangedActivityCard(arrangedActivity: activity),
            )
            .toList(),
      ),
    );
  }
}
