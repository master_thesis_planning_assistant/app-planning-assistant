import 'package:flutter/material.dart';
import 'package:planning_assistant/context/activity_plans_context.dart';
import 'package:planning_assistant/model/activity.dart';
import 'package:planning_assistant/model/activity_plan.dart';
import 'package:planning_assistant/services/backend/backend_service.dart';
import 'package:planning_assistant/widget/activity_plan_card.dart';
import 'package:provider/provider.dart';

class ActivityPlanReviewPage extends StatefulWidget {
  const ActivityPlanReviewPage(
      {Key? key,
      required this.activities,
      required this.day,
      required this.userName,
      required this.password,
      required this.filterDuplicates})
      : super(key: key);

  final List<Activity> activities;
  final DateTime day;
  final String userName;
  final String password;
  final bool filterDuplicates;

  @override
  _ActivityPlanReviewPageState createState() => _ActivityPlanReviewPageState(
      activities: activities,
      day: day,
      userName: userName,
      password: password,
      filterDuplicates: filterDuplicates);
}

class _ActivityPlanReviewPageState extends State<ActivityPlanReviewPage> {
  _ActivityPlanReviewPageState(
      {required this.activities,
      required this.day,
      required this.userName,
      required this.password,
      required this.filterDuplicates});

  final List<Activity> activities;
  final DateTime day;
  final String userName;
  final String password;
  final bool filterDuplicates;

  late Future<List<ActivityPlan>> activityPlans;

  @override
  void initState() {
    super.initState();
    activityPlans =
        getActivityPlan(activities, day, userName, password, filterDuplicates);
  }

  void onSave(ActivityPlan plan, activityPlanContext,
      List<ActivityPlan> activityPlans) {
    activityPlanContext.add(plan);
    setState(() {
      activityPlans.removeWhere((element) => element.guid == plan.guid);
    });
  }

  void onDelete(ActivityPlan plan, List<ActivityPlan> activityPlans) {
    setState(() {
      activityPlans.removeWhere((element) => element.guid == plan.guid);
    });
  }

  @override
  Widget build(BuildContext context) {
    var activityPlanContext = context.watch<ActivityPlansContext>();
    return Scaffold(
      appBar: AppBar(
        title: Text('Vorgeschlagene Aktivitätspläne'),
        centerTitle: true,
      ),
      body: FutureBuilder<List<ActivityPlan>>(
          future: activityPlans,
          builder: (BuildContext context,
              AsyncSnapshot<List<ActivityPlan>> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: const CircularProgressIndicator());
            } else {
              if (snapshot.hasError)
                return Center(child: Text('Fehler: ${snapshot.error}'));
              else if (snapshot.data == null || snapshot.data!.isEmpty) {
                return Center(child: Text('Keine Aktivitätspläne gefunden'));
              } else {
                return ListView(
                  children: snapshot.data!.map((plan) {
                    return ActivityPlanCard(
                        plan: plan,
                        savePlan: () =>
                            onSave(plan, activityPlanContext, snapshot.data!),
                        deletePlan: () => onDelete(plan, snapshot.data!));
                  }).toList(),
                );
              }
            }
          }),
    );
  }
}
