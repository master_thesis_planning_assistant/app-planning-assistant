import 'package:flutter/material.dart';
import 'package:planning_assistant/context/activity_plans_context.dart';
import 'package:planning_assistant/context/user_context.dart';
import 'package:planning_assistant/page/create_activity_plan_page.dart';
import 'package:planning_assistant/page/overview_page.dart';
import 'package:planning_assistant/page/preferences_page.dart';
import 'package:planning_assistant/page/settings_page.dart';
import 'package:provider/provider.dart';

import 'common/theme.dart';
import 'page/saved_activity_plans_page.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ActivityPlansContext()),
        ChangeNotifierProvider(create: (_) => UserContext())
      ],
      child: MaterialApp(
        title: 'Planning Assistent',
        theme: appTheme,
        initialRoute: '/overview',
        routes: {
          '/overview': (context) => OverviewPage(),
          '/activityPlans': (context) => SavedActivityPlansPage(),
          '/newActivityPlan': (context) => CreateActivityPlanPage(),
          '/preferences': (context) => PreferencesPage(),
          '/settings': (context) => SettingsPage(),
        },
      ),
    );
  }
}
